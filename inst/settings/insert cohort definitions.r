library(ROhdsiWebApi)

baseUrl <-"https://optima-rc.hzdr.de/WebAPI"

token <- 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhaGlqYXp5QGVpc2JtLm9yZyIsIlNlc3Npb24tSUQiOm51bGwsImV4cCI6MTcxNTExMDQ0Nn0.5OHbXJG5VxjbaJwgNgjE3Fpv2Mww3J3GJPFWvChzMgQkiTNkZI3SSEtirav3_EczfqOSwTdt6Bd-OZ77MaCxyw'
setAuthHeader(baseUrl = baseUrl, token)

# after inserting the cohorts
 
# Insert cohort definitions from ATLAS into package -----------------------
ROhdsiWebApi::insertCohortDefinitionSetInPackage(fileName = "inst/settings/CohortsToCreate.csv",
baseUrl = baseUrl ,
insertTableSql = TRUE,
insertCohortCreationR = TRUE,
generateStats = FALSE,
packageName = "OptimaTesting")